The ```optics_specific_tools.py``` introduces an intermediate layer between the mask and 
the optics repository, allowing the use of the same python mask with optics that require 
different manipulations and have different knob naming conventions. Examples can be found in 
[pymask examples folder](https://github.com/lhcopt/lhcmask/tree/master/python_examples).

The file should provide the following functions:

The function ```build_sequence``` receives the a MAD-X instance and builds in it 
the relevant sequences:
```python
build_sequence(mad, beam)
```
The beam to be configured is also passed to the function in order to identify 
which sequences need to be built. In particular, the sequences ```lhcb1``` and ```lhcb2``` 
are built if ```beam``` is 1 while the sequence ```lhcb2``` is built if ```beam``` is 4.
The MAD-X ```makethin``` commend can be executed and the sequence can be cycled to start away from a collision point (required for beam-beam calculations).

The function ```apply_optics``` loads the optics file given as an argument. The optics can also be
modified in the function if needed:
```python
apply_optics(mad, optics_file)
```

The function ```set_optics_specific_knobs``` sets optics knobs in the given MAD-X instance
according to the values provided in the dictionary knob_settings:
```python
set_optics_specific_knobs(mad, knob_settings, mode)
```

The function ```lumi_control``` allows implementing custom leveling methods:
```python
lumi_control(mad, twiss_dfs, configuration, knob_names)
```

The function ```twiss_and_check``` computes the twiss tables, save them on file and checks 
certain values from the twiss tables (i.e. beta functions and separations at the IPs) against the 
corresponding variables in the MAD-X namespace.
```python
twiss_and_check(mad, sequences_to_check, twiss_fname,
        tol_beta, tol_sep, save_twiss_files=True,
        check_betas_at_ips=True, check_separations_at_ips=True)
```



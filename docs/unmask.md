### Handling masked parameters

For large scans, some of the parameters in the main configuration script can be replaced by symbols (e.g. %CHROMATICITY) that will be set by [SixDesk](https://github.com/SixTrack/SixDesk).

In the following you find an example on how to handle this kind of files

Make a test folder and copy an example of main configuration file with masked parameters:

```fortran
mkdir test_folder
cd test_folder
cp /afs/cern.ch/eng/tracking-tools/modules/examples/hl_lhc_collision/main.mask .
```

To test quickly a main file with masked parameters, you can write their values in a file (e.g., ```parameters_for_unmask.txt```, you find a sample file in the example folder) with the following syntax:
```fortran
%BEAM%:1
%OCT%:100
%EMIT_BEAM:2.3
%NPART:2.25e11
%CHROM%:15
%XING:245
%SEEDRAN:1
```

!!! warning
    The values used in the example above are arbitrary. Each user is responsible of the configuration settings.

The you can use the unmask script available in the repository to generate the unmasked version of the file:
```fortran
python /afs/cern.ch/eng/tracking-tools/modules/unmask.py main.mask parameters_for_unmask.txt
```      

This generates an unmasked version of the file (main.mask.unmasked), which can be executed by madx:
```fortran
madx main.mask.unmasked
```
# **Step-by-step description**

The main steps performed by the code, that can be easily identified in the main file ([```000_pymask.py```](https://github.com/lhcopt/lhcmask/blob/master/python_examples/hl_lhc_collisions_python/000_pymask.py)),
are the following:

1. **Load configuration** -  The code loads the used-defined input from ```config.py``` and prepares the running environment.
2. **Select mode** - The selected mode is activated, defining which beam will be tracked - b1 or b4 - and whether beam-beam needs to be included
   (more information on the available modes is available in the [documentation of
    the config.py file](config_py_doc.md)).
3. **Start MAD-X** - A MAD-X process is started through [cpymad](cpymad_basics.md).
4. **Build sequences** - The accelerator sequences are built by calling the user-defined function ```build_sequence```
   from the file [```optics_specific_tools.py```](ost_py_doc.md). Even if b4 is to be tracked, at this stage the sequences for b1 and b2 are generated to compute the information to configure the beam-beam 
   encounters (the b4 sequence is generated later if needed).
5. **Load optics** - The optics is loaded by calling the user-defined function ```apply_optics```
   from the file [```optics_specific_tools.py```](ost_py_doc.md). 
   are copied to the MAD-X global variable space.
6. **Beam definitions** - Beam definitions are attached to the MAD-X sequences.
7. **Optics sanity checks** - The twiss tables are computed and sanity checks are performed by calling the user-defined function ```twiss_and_check```
   from the file [```optics_specific_tools.py```](ost_py_doc.md).
8. **Load optics-specific knobs** - Optics-specific knobs (e.g. crossing angles, separations at the IPs, dispersion correction) are set by 
   calling the user-defined function ```set_optics_specific_knobs``` from the file [```optics_specific_tools.py```](ost_py_doc.md).
9. **Set phase advances** - The phase advances among IPs is adjusted.
10. **Save knob references** - Reference values for orbit knobs are saved by calling the MAD-X macro ```crossing_save``` (discussed [here](pymask_structure_interf.md)).
11. **Checks** - The following checks are performed:

    *  The closed orbit is made flat by using the MAD-X macro ```crossing_disable``` (discussed [here](pymask_structure_interf.md)) 
    and the optics in this configuration is rechecked by calling the user-defined function ```twiss_and_check```. 
    The flatness of the orbit is also verified.
    * The closed orbit is restored by using the MAD-X macro ```crossing_restore```  (discussed [here](pymask_structure_interf.md)) 
    and the optics in this configuration is rechecked by calling the user-defined function ```twiss_and_check```. 
    
12. **Luminosity control** - The luminosity at the IPs is modified (for example through separation leveling) by calling the user-defined 
    function ```lumi_control``` from the file [```optics_specific_tools.py```](ost_py_doc.md) and 
    reference values for orbit knobs are re-saved by calling the MAD-X macro ```crossing_save```. 
    The optics in this configuration is rechecked by calling the user-defined function ```twiss_and_check```. 
13. **Generate beam-beam information** - Pandas dataframes are generated, which contain the information on the beam-beam encounters for the clockwise beams (b1, b2) 
    and for the anticlockwise beams (b3, b4).
14. **Generate MAD-X instance for beam 4** - If b4 is to be tracked, a second instance of MAD-X is launched and the sequence of 
    b4 is built by calling once more the user-defined function ```build_sequence```
    from the file [```optics_specific_tools.py```](ost_py_doc.md). The configuration of b2 from the first MAD-X instance
    is then transferred to b4.
    The optics for b2 is rechecked by calling the user-defined function ```twiss_and_check```.
15. **Select MAD-X instance for tracking** - The MAD-X instance with the sequence to be tracked is selected. It is rechecked 
    by calling the user-defined function ```twiss_and_check```.
16. **Install beam-beam lenses** - Beam-beam lenses are installed in the sequence to be tracked based on thee dataframes that
    had been previously generated. They are left disabled and will be enabled later.
17. **Install crab cavities** - Crab cavities are installed in the sequences (but left inactive) and the crabbing correctors (used to mimimic the crab bump with the MAD-X twiss) are removed.
18. **Disable dispersion correction** - The dispersion correction knob is set to zero.
19. **Last use** - The last "use" command is performed (then it needs to be avoided not to erase the multipolar 
    and alignment error information).
20. **Save references** - Reference parameters from the twiss tables are saved for corrections.
21. **Machine imperfection, installation and correction** - Machine imperfections are introduced and corrected. 
22. **Generate linear-coupling knobs** - Linear-coupling knobs are generated.
    * A ```crossing_restore``` is executed, which also restores the dispersion correction knobs. 
23. **Activate octupoles** - Landau Octupole magnets are introduced.
24. **Correct linear coupling** - The linear coupling is corrected.
25. **Match closed orbit** - The closed orbit is rematched using the references saved before.
26. **Match tunes and chromaticities** - The tunes and chromaticities are re-matched to the values provided in input.
27. **Switch on beam-beam lenses** - The beam-beam lenses are switched on.
28. **Switch on RF** - The accelerating cavities are switched on.
29. **Switch on crab cavities** - The crab cavities are switched on.
30. **Generate sixtrack input** - The sixtrack input is generated from the MAD-X model. The beam-beam information for the sixtrack 
    input files is obtained from the pandas dataframes generated before.
31. **Save closed orbit and optics at start ring** - The closed orbit and the optics at the first element of the ring are saved, 
    for the generation of matched particle distributions.
32. **Generate pysixtrack line** - The pysixtrack model is generated and saved on file.


# **Running a simple example**

If you don't want to spend time to setup the environment, and you have access to the CERN lxplus system, you can login on a *lxplus* machine and execute 
```bash
source /afs/cern.ch/eng/tracking-tools/python_installations/activate_default_python  
```
This activates a python installation with the required packages and tools.

If, instead,  you want to use a different computer or have a more customizable environment you 
can configure your own python environment following the instructions available 
[here](setting_local_python.md).

Once your python environment is activated you can clone the  [*lhcmask*](https://github.com/lhcopt/lhcmask){target=_blank} repository
in your local folder
```bash
git clone https://github.com/lhcopt/lhcmask.git
```
and move to an example folder.
```bash
cd ./lhcmask/python_examples/hl_lhc_collisions_python
```
You can run pymask by simply typing:
```bash
python 000_pymask.py
```

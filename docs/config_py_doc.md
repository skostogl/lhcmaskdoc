# **The main configuration file (config.py)**

The ```config.py``` file contains the main simulation settings and parameters as well 
as the optics-specific knob names. It consists of a single dictionary named ```configuration```.

Examples can be found in the [pymask examples folder](https://github.com/lhcopt/lhcmask/tree/master/python_examples).

The detailed content of the ```configuration``` dictionary will be discussed in the following.

The parameter ```mode``` defines which beam needs to be configured for tracking, 
whether beam-beam effects should be included, and what workflow is followed. The following 
modes can be used for the simulation of the clockwise beam (beam 1):
```python
'b1_without_bb'
'b1_with_bb'
```
while the following modes can be used for the simulation of the anticlockwise beam (beam 4):
```python
'b4_from_b2_without_bb'
'b4_from_b2_with_bb'
```
The following two modes are also available mainly for testing purposes (some features may not be available when they are selected): 
```python
'b1_with_bb_legacy_macros'
'b4_without_bb'
```

The parameter ```links``` defines links to the required tools and repositories:
```python
'links'   : {
          'tracking_tools': '/afs/cern.ch/eng/tracking-tools',
          'modules': 'tracking_tools/modules',
          'tools': 'tracking_tools/tools',
          'beambeam_macros': 'tracking_tools/beambeam_macros',
          'errors': 'tracking_tools/errors',
          },

```

The optics file to be loaded is defined by:
```python
'optics_file'              : '/afs/cern.ch/eng/lhc/optics/HLLHCV1.4/round/opt_round_150_1500_thin.madx', #15 cm
```

The automatic checks and the saving of twiss tables on parquet file, at different stages 
of the computation (for checks) can be enabled using the following parameters:
```python
'check_betas_at_ips'       : True,
'check_separations_at_ips' : True,
'save_intermediate_twiss'  : True,
```

Tolerances for the automatic checks on beta functions and separations at the IPs are 
defined by the following parameters:
```python
# Tolerances for checks [ip1, ip2, ip5, ip8]
'tol_beta'                 : [1e-3, 10e-2, 1e-3, 1e-2],
'tol_sep'                  : [1e-6, 1e-6, 1e-6, 1e-6],
```

The tolerance to check the machine flatness (after the ```crossing_disable``` macro is executed) is defined by:
```python
'tol_co_flatness'          : 1e-6,
```
The beam parameters (used to configure the MAD-X beams) are the following:
```python
'beam_norm_emit_x'     : 2.5,          # [um]
'beam_norm_emit_y'     : 2.5,          # [um]
'beam_sigt'            : 0.076,        # [m]
'beam_sige'            : 1.1e-4,       # [-]
'beam_npart'           : 2.2e11,       # [-]
'beam_energy_tot'      : 7000,         # [GeV]
```

   
Machine settings like tunes, chromaticities, rf voltage, octupole currents are set 
through the following parameters:
```python
# Tunes and chromaticities
'qx0'                  : 62.31,
'qy0'                  : 60.32,
'chromaticity_x'       : 5,            # [-] 
'chromaticity_y'       : 5,            # [-] 

# RF voltage
'vrf_total'            : 16.,          # [MV]

# Octupole current
'oct_current'          : -235,         # [A]
```

The "luminosity control" features are controlled through the following parameters:
```python
# Luminosity parameters
'enable_lumi_control'      : True,
'sep_plane_ip2'            : 'x', # used by python tools - NOT by legacy macros
'sep_plane_ip8'            : 'y', # used by python tools - NOT by legacy macros
'lumi_ip8'             : 2e33, # [Hz/cm2] leveled luminosity in IP8 
'fullsep_in_sigmas_ip2': 5,
'nco_IP1'              : 2748, # number of Head-On collisions in IP1
'nco_IP2'              : 2494, # number of Head-On collisions in IP1
'nco_IP5'              : 2748, # number of Head-On collisions in IP1
'nco_IP8'              : 2572, # number of Head-On collisions in IP1
```
The luminosity control (e.g. through separation leveling) can be flexibly defined by 
the user through the function ```lumi_control``` in the 
[optics-specific tools](ost_py_doc.md) file.

The configuration of the beam-beam encounters is done through the following 
parameters:
```python
# Beam-beam parameters
'numberOfLRPerIRSide'      : [25, 20, 25, 20],
'bunch_spacing_buckets'    : 10,
'numberOfHOSlices'         : 11,
'bunch_population_ppb'     : None,
'sigmaz_m'                 : None,
```
The head-on encounters are modelled by a set of 6D beam-beam lenses, each associated to one
longitudinal slice of the strong beam (constant-charge slicing is applied). 
The procedure used to configure the beam-beam lenses
based on the MAD-X model of the collider is described in detail in [this document](/draft_bb_pymask_note.pdf).

The z-dependent orbit distortion introduced by crab cavities is calculated using the MAD-X twiss, 
using orbit correctors to simulate the effect of the crab cavities. These correctors are supposed to be 
installed in the loaded sequence and are controlled with a knob called ```z_crab``, which defines
the z-position along the bunch to be simulated by the correctors.
The following input parameter defines the z coordinate for which the crab bump is computed:
```python
'z_crab_twiss'             : 0.075,
```
The RF curvature, is applied by rescaling the bump with a sine function (as 
discussed [here](/draft_bb_pymask_note.pdf)).

The following boolean parameter defines whether the matching of the tunes and chromaticities
is performed taking into account beam-beam effects:
```python
'match_q_dq_with_bb'        : False,            
```

The following boolean parameter allows disabling the installation of the crab cavities:
```python
'enable_crabs'             : True,
```

The following settings are related to the linear-coupling correction, allowing to set 
the number of iterations used to minimize the linear coupling and the values added 
after this correction to the linear coupling knobs.
```python
# N. iterations coupling correction
'N_iter_coupling'            : 2,

# Value to be added to linear coupling knobs (on sequence_to_track)
'delta_cmr'                 : 0.,
'delta_cmi'                 : 0.,
```

The following parameter enables the print of extra information by the MAD-X parts:
```python 
'verbose_mad_parts'         : True,
```
The ```knob_names``` dictionary allows providing the names for knobs that need to be  used by the pymask code:
```python
'knob_names' : {
  # Common knobs
  'sepknob_ip2_mm': 'on_sep2',
  'sepknob_ip8_mm': 'on_sep8',

  # Knobs associated to sequences
  'qknob_1': {'lhcb1': 'kqtf.b1',  'lhcb2':'kqtf.b2'},
  'qknob_2': {'lhcb1': 'kqtd.b1',  'lhcb2':'kqtd.b2'},
  'chromknob_1': {'lhcb1': 'ksf.b1',  'lhcb2':'ksf.b2'},
  'chromknob_2': {'lhcb1': 'ksd.b1',  'lhcb2':'ksd.b2'},
  'cmrknob': {'lhcb1': 'cmrskew',  'lhcb2':'cmrskew'},
  'cmiknob': {'lhcb1': 'cmiskew',  'lhcb2':'cmiskew'},
  },
```

In particular, the following parameters define the names of the knobs controlling the parallel
separation at the IPs:
```python
'sepknob_ip2_mm': 'on_sep2',
'sepknob_ip8_mm': 'on_sep8',
```
the names of two independent knobs that can be used to match the tunes are defined, for 
the two beams independently, as follows:
```python
'qknob_1': {'lhcb1': 'kqtf.b1',  'lhcb2':'kqtf.b2'},
'qknob_2': {'lhcb1': 'kqtd.b1',  'lhcb2':'kqtd.b2'},
```
the names of two independent knobs that can be used to match the chromaticities are defined, for 
the two beams independently, as follows:
```python
'chromknob_1': {'lhcb1': 'ksf.b1',  'lhcb2':'ksf.b2'},
'chromknob_2': {'lhcb1': 'ksd.b1',  'lhcb2':'ksd.b2'},
```

the names of the "cmr" and "cmi" knobs that can be used to control the linear coupling are defined by:
```python
'cmrknob': {'lhcb1': 'cmrskew',  'lhcb2':'cmrskew'},
'cmiknob': {'lhcb1': 'cmiskew',  'lhcb2':'cmiskew'},
```
Such knobs can be available directly from the optics file or generated by the mask
(if the flag ```enable_knob_synthesis``` is enabled and the optics toolkit provides the required functionalities).

The ```knob_settings``` dictionary contains the values to be set for certain optics-dependent knobs.
The setting of these knobs is performed through the function ```set_optics_specific_knobs``` in the [optics-specific tools](ost_py_doc.md) file and not directly by the mask. As a consequence, different knob names can be used in different optics, without changing anything else than this function.

The only knobs that are used directly by the mask are ```on_disp``` which controls the dispersion correction bump (if present) and ```on_crab1```/```on_crab5``` which control the crab cavities (if present).

An example of knob_settings dictionary is shown in the following.
```python
'knob_settings':  {
  #IP specific orbit settings
  'on_x1'                   : 250,          # [urad]  
  'on_sep1'                 : 0,            # [mm]   
  'on_x2'                   : -170,         # [urad] 
  'on_sep2'                 : 0.138,        # [mm]   
  'on_x5'                   : 250,          # [urad] 
  'on_sep5'                 : 0,            # [mm]   
  'on_x8'                   : -250,         # [urad] 
  'on_sep8'                 : -0.043,       # [mm]   
  'on_a1'                   : 0,            # [urad] 
  'on_o1'                   : 0,            # [mm]   
  'on_a2'                   : 0,            # [urad] 
  'on_o2'                   : 0,            # [mm]   
  'on_a5'                   : 0,            # [urad] 
  'on_o5'                   : 0,            # [mm]   
  'on_a8'                   : 0,            # [urad] 
  'on_o8'                   : 0,            # [mm]   
  'on_crab1'                : -190,         # [urad] 
  'on_crab5'                : -190,         # [urad]  

   # Dispersion correction knob
   'on_disp'                 : 1,            # Value to choose could be optics-dependent

    # Magnets of the experiments
    'on_alice_normalized'     : 1,
    'on_lhcb_normalized'      : 1,

    'on_sol_atlas'            : 0,
    'on_sol_cms'              : 0,
    'on_sol_alice'            : 0,
    },
```

The following boolean parameters allow disabling the installation and correction of 
machine imperfections (for now available only for HL-LHC optics) and the synthesis of 
the linear coupling knobs:
```python
    # Enable machine imperfections
    'enable_imperfections'     : True,

    # Enable knob synthesis (for coupling correction, if no imperfections)
    'enable_knob_synthesis'    : True,
```
The following dictionary can be used to control the installation and correction of
multipolar and alignment errors:
```python
    # Parameters for machine imperfections
    'pars_for_imperfections': {
        'par_myseed'               : 1,
        'par_correct_for_D2'       : 0,
        'par_correct_for_MCBX'     : 0,
        'par_on_errors_LHC'        : 1,
        'par_on_errors_MBH'        : 1,
        'par_on_errors_Q5'         : 1,
        'par_on_errors_Q4'         : 1,
        'par_on_errors_D2'         : 1,
        'par_on_errors_D1'         : 1,
        'par_on_errors_IT'         : 1,
        'par_on_errors_MCBRD'      : 0,
        'par_on_errors_MCBXF'      : 0,
        },
```

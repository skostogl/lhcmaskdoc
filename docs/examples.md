In this page we will collect some detailed examples of a pythonic approach to the LHC and HL-LHC masks.

!!! info
    This is the recipe to install the needed python package.
    We assume that you have access to afs and to the internet.
    
    ### The shortcut
    
    Open your lxplus terminal and enter
    ```
    source /afs/cern.ch/eng/tracking-tools/python_installations/activate_default_python  
    ```
    and that's it.
    
    ### The standard way
    
    We suggest you setup your own installation (you will be more aware of the configuration and you will be customize it).
    
    To setup your environment you can install a [miniconda](https://docs.conda.io/en/latest/miniconda.html) distribution. E.g.,
    
    ```
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    sh Miniconda3-latest-Linux-x86_64.sh
    ```
    and follow the instructions. 
    
    After having activated it, install the needed packages by
    ```bash
    pip install ipython numpy pandas scipy pyarrow matplotlib cpymad
    ```
    Then install
    ```
    pip install pyNAFF
    ```
    
    In addition you need to install
    
    - pysixtrack
    ```bash
    git clone https://github.com/SixTrack/pysixtrack.git
    cd pysixtrack
    pip install -e .
    cd ..
    ```
    
    - sixtracktools
    ```bash
    git clone https://github.com/SixTrack/sixtracktools.git
    cd sixtracktools
    pip install -e .
    cd ..
    ```
    
    - sixtracklib. This requires a recent version of *cmake*. If you are installing on your own machine it should not be a problem.
    On lxplus, you call another environment before the installation (thanks to K. Paraschou, G. Iadarola).
    
    ```bash
    source /cvmfs/sft-nightlies.cern.ch/lcg/views/dev4cuda9/latest/x86_64-centos7-gcc62-opt/setup.sh
    unset PYTHONHOME
    unset PYTHONPATH
    ```
    
    then you have to go back to your python distribution. E.g., **please replace conveniently the paths below** 
    ```bash
    source /afs/cern.ch/eng/tracking-tools/python_installations/miniconda3/bin/activate
    export PATH=/afs/cern.ch/eng/tracking-tools/python_installations/miniconda3/bin:$PATH
    ```
    
    Please check with
    ```bash
    which python ipython cmake
    ```
    
    that *python* and *ipython* are pointing to your miniconda and that *cmake* is pointing to 
    ```
    /cvmfs/sft-nightlies.cern.ch/lcg/views/dev4cuda9/Tue/x86_64-centos7-gcc62-opt/bin/cmake
    ```
    
    Then you can proceed with
    ```bash
    git clone https://github.com/SixTrack/sixtracklib.git
    cd sixtracklib
    mkdir build
    cd build
    cmake ..
    make
    cd ../python
    pip install -e .
    cd ..
    ```
    
    ### Test it
    
    You can download the *lhcmask* package.
    
    ```bash
    git clone https://github.com/lhcopt/lhcmask.git
    ```
    
    It contains the examples that you are going to test. In particular you can now test a pure pythonic mask by 
    ```
    cd lhcmask/python_examples/hl_lhc_collisions_python
    python 000_pymask.py
    ```
    or you can run a standard MAD-X file by
    ```
    cd lhcmask/examples/hl_lhc_collision
    python ../../unmask.py main.mask parameters_for_unmask.txt --run
    ```
    
    


Each example is the markdown version of a jupyter notebook: you can read the markdown but you are also inveted to download and execute the notebook (at the start of the markdown document you will have the link to download the notebook).
The full list of notebook is available [here](https://gitlab.cern.ch/sterbini/lhcmaskdoc/-/tree/master/docs%2Fipynbs) and you can get them by cloning this repository

```
git clone https://gitlab.cern.ch/sterbini/lhcmaskdoc.git
```

Thank you for contributing.

### A python mask for Run3

You can find [here](ipynbs/python_approach_run3/A_python_mask_for_Run_3.md) an example of python mask for Run3.

### A python function for the luminosity leveling

You can find [here](ipynbs/luminosity_formula/luminosity_formula.md) a detailed description of the luminosity function we propose. An example its use can be found [here](ipynbs/python_example_simple/Pythonic_approach_simple.md).

We suggest also this interesting [resource](https://github.com/stpapado/qGaussian_Luminosity) by S. Papadopoulou.

### Installation of the wires 

You can find [here](ipynbs/wires_run3/b1_checks.md) an example of installation of the wires for LHC Run 3, with several sanity checks. 

### MADX B2 vs B4

You can find [here](ipynbs/make_b1_b2_b3_b4/Beam_2_and_Beam_4.md) an example of the specifity of the MADX B2 vs B4. 

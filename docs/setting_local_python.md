## **Shared python installation on lxplus**

If you do not need a special configuration you can just use the standard lxplus configuration.

Open your lxplus terminal and type
```bash
source /afs/cern.ch/eng/tracking-tools/python_installations/activate_default_python  
```
and that's it.


## **Setup your own python installation**

If you need to be more aware of the configuration and customize it, you can use the following instructions to setup your own environment.

Install a [miniconda](https://docs.conda.io/en/latest/miniconda.html) distribution. E.g. by

```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
sh Miniconda3-latest-Linux-x86_64.sh
```
and follow the instructions. 

After having activated it, install the needed packages by
```bash
pip install ipython numpy pandas scipy pyarrow matplotlib cpymad
```
Then install
```
git clone https://github.com/PyCOMPLETE/NAFFlib.git
cd  NAFFlib
pip install -e .
cd NAFFlib
make py3
cd ../../
```

In addition you need to install [xsuite](https://xsuite.readthedocs.io) :

``` bash
pip install xsuite
```

- sixtracktools

```bash
git clone https://github.com/SixTrack/sixtracktools.git
cd sixtracktools
pip install -e .
cd ..
```

At this point the installation is complete and can be tested by running one of 
the pymask examples, following the instructions available 
[here](pymask_run_example.md).

